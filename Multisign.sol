pragma solidity 0.4.24;

import "./Bundle.sol";

/**
 * @title Multisign
 * @notice This contract is used for storing funds when an ICO
 * @notice is finished until the founders call for a whitdrawal with consensus.
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract Multisign is Bundle {
    // **** DATA

    mapping (address => uint256) public hasSigned;


    // **** MODIFIERS

    // @notice To limit functions usage to founders
    modifier onlyFounders() {
        require(msg.sender == walletFounder1 ||
            msg.sender == walletFounder2 ||
            msg.sender == walletFounder3 ||
            msg.sender == walletFounder4 ||
            msg.sender == walletFounder5);
        _;
    }


    // **** FUNCTIONS

    /**
     * @notice Receives the funds of ICO, from the block.timestampndVault
     */
    function deposit() public payable returns (bool) {
        emit Deposit(msg.value, block.timestamp); // Audit log

        return true;
    }

    /**
     * @notice Called by founders to sign and get funds
     * @return true if withdrawal was performed successfully
     * @return false when just signing and when transfer fails on withdrawal
     */
    function withdraw() public onlyFounders returns (bool) {
        // Founder signature
        hasSigned[msg.sender] = 1;
        emit WithdrawSign(msg.sender, block.timestamp); // Event log

        // Check whether funds must be sent
        if (
        hasSigned[walletFounder1] > 0 && /* compulsory sign of founder #1 */
        hasSigned[walletFounder2].add(hasSigned[walletFounder3]).add(hasSigned[walletFounder4]).add(hasSigned[walletFounder5]) > 1 /* compulsory sign of at least 2 founders different from CEO */
        ) {
            // Send funds to founders' wallets
            uint256 weiAmount = address(this).balance.mul(20).div(100); // 1st wallet gets 20%
            walletFounder1.transfer(weiAmount);
            emit Withdraw(walletFounder1, weiAmount); // Event log

            weiAmount = address(this).balance.mul(25).div(100); // 2nd wallet gets another 20%
            walletFounder2.transfer(weiAmount);
            emit Withdraw(walletFounder2, weiAmount); // Event log

            weiAmount = address(this).balance.mul(33).div(100); // 3rd wallet gets another 20%
            walletFounder3.transfer(weiAmount);
            emit Withdraw(walletFounder3, weiAmount); // Event log

            weiAmount = address(this).balance.mul(50).div(100); // 4th wallet gets another 20%
            walletFounder4.transfer(weiAmount);
            emit Withdraw(walletFounder4, weiAmount); // Event log

            weiAmount = address(this).balance; // 5th wallet gets the resting 20%
            walletFounder5.transfer(weiAmount);
            emit Withdraw(walletFounder5, weiAmount); // Event log

            // Funds has been sent, now reset founders' signatures
            hasSigned[walletFounder1] = 0;
            hasSigned[walletFounder2] = 0;
            hasSigned[walletFounder3] = 0;
            hasSigned[walletFounder4] = 0;
            hasSigned[walletFounder5] = 0;

            return true;
        }

        return false;
    }

    // **** EVENTS

    // Triggered when getting funds from ICO/RefundVault
    event Deposit(uint256 _amount, uint256 _timestamp);

    // Triggered when a founder signs
    event WithdrawSign(address indexed _aFounder, uint256 _timestamp);

    // Triggered when sending contract funds
    event Withdraw(address indexed _finalWallet, uint256 _weiAmount);
}
