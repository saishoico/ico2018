// Tokens.sol

/*
SaishoArt Token Smart Contracts v1.3
developed by:
    MarketPay.io , 2018
    https://marketpay.io/
    https://goo.gl/kdECQu
    
    v1.3
    + added Saisho hardcoded wallets
    + revert('errorString') instead of error('errorString')
    + force compiling with Solidity 0.4.24
    + splitted code in a file per contract scheme
    + improved variable names and declarations on ERC-20 compliancy
    + removed transferOrigin method for audit validation
    + improved deployment forcing whitelisting hardcoded wallets by owner with Whitelist.setInitialWallets()
    + getBackTokensOnRefund() now uses ERC-20 allowance scheme to recover the investor tokens
    
    v1.2
    + bug fix: minInvestment = 1 euro
    + rename Saisho Token symbol to STN
    + renamed typo VaultRefund to RefundVault
    + ICO payments through Escrow contract, ruled by ComplianceOfficer oracle
    + ICO contract exceeds Ropsten gasLimit, refactored
        * removed function getDiscount (instead, inline discount calculation)
        * removed function hasEnded()
        * removed functionality v1.1 Decreased weisRaised when refunding
        * claimRefund from RefundVault instead from ICO
        * oracleTakeOut not from ICO anymore, just takeOut from RefundVault
    + tags: _at_dev => @notice
    + revert("Static custom error string") // seems working on Solidity 0.4.23
    + ICO softCap => 81,018 ST
    
    v1.1
    + Force compiling with Solidity 0.4.23
    + Removed MarketPay cut
    + ST is 1 euro instead of 1 ether
        weisMinInvestment to 1 euro too
    + External calls:
        * replaced transfer with send and error exception
        * transfer not replaced at
            RefundVault.refund()
          to force revert() on failure. Otherwise a token/ether misbalance would arise
        * Methods including this dangerous calls are:
            RefundVault.close()
            Multisign.withdraw()
    + Added tags @author @dev @return
    + Added extra tokens to founders: On finalize() mint and transfer 10% of the tokens sold to founder#1 wallet
    + Added RefundVault.takeOut() and ICO.oracleTakeOut() oracle endpoint
    + Decreased weisRaised when refunding
    + Added notnull address contracts on constructors
    + Whitelist address extracted from Tokens contract on ICO constructor
    + Added new contract to hardcode oracle wallets
        * From now on, ComplianceOfficer is the only one that should construct the Whitelist
    + After founder's withdrawal reset signatures
    + Updated discounts from percentage to per mille and new values
    
    v1.0
    + Contracts: Whitelist, Tokens, Multisign, RefundVault, ICO

*/

pragma solidity 0.4.24;

import "./Bundle.sol";
import "./ERC20.sol";
import "./Whitelist.sol";
import "./ITokens.sol";


/**
 * @title Tokens
 * @notice ERC20 implementation of ST tokens
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract Tokens is Bundle, ERC20, ITokens {
    // **** DATA
    
    mapping (address => uint256) private balances;
    mapping (address => mapping (address => uint256)) private allowed;
    uint256 public initialSupply; // Initial and total token supply
    uint256 public totalSupply_; // initialSupply differ from totalSupply when there is a mint functionality

    // Public variables of the token, all used for display
    string public name;
    string public symbol;
    uint8 public decimals;
    string public standard = "H0.1"; // HumanStandardToken is a specialisation of ERC20 defining these parameters

    // Timelock
    uint256 public timelockEndTime;


    // **** MODIFIERS

    // @notice To limit token transfers while timelocked, only ICOs are allowed to transfer from or to
    modifier notTimeLocked(address _to) {
        require(block.timestamp >= timelockEndTime ||
            msg.sender == AICO || msg.sender == AEscrow || msg.sender == ARefundVault ||
            msg.sender == AReSale ||
            msg.sender == walletFiat ||
            _to == AICO || _to == AEscrow || _to == ARefundVault ||
            _to == AReSale ||
            _to == walletFiat);
        _;
    }


    // **** FUNCTIONS

    /**
     * @notice Constructor: set up token properties
     */
    constructor() public {
        name = "SaishoToken";
        symbol = "STN";
        decimals = 0; // tokens are indivisible units

        initialSupply = 0; // ST tokens are minted as long ICO investors buy them
        totalSupply_ = initialSupply;

        // ICO Fiat requires premined tokens
        uint256 amountPreminedTokens = 75000000; // 75M. These tokens are sent to fiat wallet ready to spend on fiat investors
        totalSupply_ = totalSupply_.add(amountPreminedTokens);
        balances[walletFiat] = balances[walletFiat].add(amountPreminedTokens);
        emit Transfer(0x0, walletFiat, amountPreminedTokens); // Event log

        timelockEndTime = block.timestamp.add(180 days); // Default timelock
    }

    /**
     * @notice Get the token balance of a wallet with address _tokenOwner
     * @return token balance of _tokenOwner
     */
    function balanceOf(address _tokenOwner) public view returns (uint256) {
        return balances[_tokenOwner];
    }

    /**
     * @notice Send _amount amount of tokens to address _to
     * @return true if transfer was successful
     */
    function transfer(address _to, uint256 _amount) public notTimeLocked(_to) stopInEmergency returns (bool) {
        require(balances[msg.sender] >= _amount);
        require(Whitelist(AWhitelist).isWhitelisted(_to));
        balances[msg.sender] = balances[msg.sender].sub(_amount);
        balances[_to] = balances[_to].add(_amount);

        emit Transfer(msg.sender, _to, _amount); // Event log

        return true;
    }

    /**
     * @notice Send _amount amount of tokens from address _from to address _to
      * @notice The transferFrom method is used for a withdraw workflow, allowing contracts to send
      * @notice tokens on your behalf, for example to "deposit" to a contract address and/or to charge
      * @notice fees in sub-currencies; the command should fail unless the _from account has
      * @notice deliberately authorized the sender of the message via some mechanism
     * @return true if transfer was successful
      */
    function transferFrom(address _from, address _to, uint256 _amount) public notTimeLocked(_to) stopInEmergency returns (bool) {
        require(balances[_from] >= _amount);
        require(allowed[_from][msg.sender] >= _amount);
        require(Whitelist(AWhitelist).isWhitelisted(_to));

        balances[_from] = balances[_from].sub(_amount);
        balances[_to] = balances[_to].add(_amount);
        allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_amount);

        emit Transfer(_from, _to, _amount); // Event log

        return true;
    }

    /**
     * @notice Allow _spender to withdraw from your account, multiple times, up to the _amount amount.
      * @notice If this function is called again it overwrites the current allowance with _amount.
     */
    function approve(address _spender, uint256 _amount) public returns (bool) {
        allowed[msg.sender][_spender] = _amount;
        emit Approval(msg.sender, _spender, _amount); // Event log

        return true;
    }

    /**
     * @notice Returns the amount which _spender is still allowed to withdraw from _tokenOwner
     */
    function allowance(address _tokenOwner, address _spender) public view returns (uint256) {
        return allowed[_tokenOwner][_spender];
    }

    /**
    * @notice Function to mint tokens
    * @param _amount The amount of tokens to mint.
    * @return true if operation was successful.
    */
    function mint(uint256 _amount) external onlyICOReSale returns (bool) {
        totalSupply_ = totalSupply_.add(_amount);
        balances[msg.sender] = balances[msg.sender].add(_amount);

        emit Mint(msg.sender, _amount); // Event log
        emit Transfer(address(0), msg.sender, _amount); // Event log

        return true;
    }

    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }

    // **** EVENTS
    
    // Triggered when tokens are transferred
    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    
    // Triggered when a wallet approves a spender to move its tokens
    event Approval(address indexed _tokenOwner, address indexed _spender, uint256 _amount);
    
    // Triggered when tokens are minted
    event Mint(address indexed _to, uint256 _amount);
}
