pragma solidity 0.4.24;

// Interface of ERC20 token contract => https://theethereum.wiki/w/index.php/ERC20_Token_Standard
interface ERC20 {
    function balanceOf(address _tokenOwner) external view returns (uint256);
    function transfer(address _to, uint256 _amount) external returns (bool);
    function transferFrom(address _from, address _to, uint256 _amount) external returns (bool);
    function allowance(address _tokenOwner, address _spender) external view returns (uint256);
    function approve(address _spender, uint256 _amount) external returns (bool);
    
    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    event Approval(address indexed _tokenOwner, address indexed _spender, uint256 _amount);
}
