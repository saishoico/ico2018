pragma solidity 0.4.24;

import "./Bundle.sol";
import "./ITokens.sol";
import "./Whitelist.sol";


/**
 * @title PreTokens
 */
contract TokensPreSale is Bundle, ITokens {
    // **** DATA
    
    mapping (address => uint256) private balances;
    uint256 public initialSupply; // Initial and total token supply
    uint256 public totalSupply_; // initialSupply differ from totalSupply_ when there is a mint functionality

    // Public variables of the token, all used for display
    string public name;
    string public symbol;
    uint8 public decimals;
    string public standard = "H0.1"; // HumanStandardToken is a specialisation of ERC20 defining these parameters

    // **** MODIFIERS

    // @notice To limit token transfers while timelocked, only ICOs are allowed to transfer from or to
    modifier notLocked(address _to) {
        require(msg.sender == AICO || msg.sender == AEscrow || msg.sender == ARefundVault ||
        msg.sender == AReSale || msg.sender == walletFiat ||
        _to == AICO || _to == AEscrow || _to == ARefundVault ||
        _to == AReSale || _to == walletFiat);
        _;
    }


    // **** FUNCTIONS

    /**
     * @notice Constructor: set up token properties
     */
    constructor() public {
        name = "SaishoTokenPreSale";
        symbol = "STP";
        decimals = 0; // tokens are indivisible units

        initialSupply = 0; // ST tokens are minted as long ICO investors buy them
        totalSupply_ = initialSupply;

        // ICO Fiat requires premined tokens
        uint256 amountPreminedTokens = 75000000; // 75M. These tokens are sent to fiat wallet ready to spend on fiat investors
        totalSupply_ = totalSupply_.add(amountPreminedTokens);
        balances[walletFiat] = balances[walletFiat].add(amountPreminedTokens);
        emit Transfer(0x0, walletFiat, amountPreminedTokens); // Event log

    }

    /**
     * @notice Get the token balance of a wallet with address _tokenOwner
     * @return token balance of _tokenOwner
     */
    function balanceOf(address _tokenOwner) public view returns (uint256) {
        return balances[_tokenOwner];
    }

    /**
    * @notice Function to mint tokens
    * @param _amount The amount of tokens to mint.
    * @return true if operation was successful.
    */
    function mint(uint256 _amount) external onlyICO returns (bool) {
        totalSupply_ = totalSupply_.add(_amount);
        balances[msg.sender] = balances[msg.sender].add(_amount);

        emit Mint(msg.sender, _amount); // Event log
        emit Transfer(address(0), msg.sender, _amount); // Event log

        return true;
    }

    /**
     * @notice Send _amount amount of tokens to address _to
     * @return true if transfer was successful
     */
    function transfer(address _to, uint256 _amount) public notLocked(_to) stopInEmergency returns (bool) {
        require(balances[msg.sender] >= _amount);
        require(Whitelist(AWhitelist).isWhitelisted(_to));
        balances[msg.sender] = balances[msg.sender].sub(_amount);
        balances[_to] = balances[_to].add(_amount);

        emit Transfer(msg.sender, _to, _amount); // Event log

        return true;
    }


    /**
     * @notice Send _amount amount of tokens from address _from to address _to
      * @notice The transferFrom method is used for a withdraw workflow, allowing contracts to send
      * @notice tokens on your behalf, for example to "deposit" to a contract address and/or to charge
      * @notice fees in sub-currencies; the command should fail unless the _from account has
      * @notice deliberately authorized the sender of the message via some mechanism
     * @return true if transfer was successful
      */
    function transferFrom(address _from, address _to, uint256 _amount) public notLocked(_to) stopInEmergency returns (bool) {
        require(balances[_from] >= _amount);
        require(Whitelist(AWhitelist).isWhitelisted(_to));
        
        balances[_from] = balances[_from].sub(_amount);
        balances[_to] = balances[_to].add(_amount);

        emit Transfer(_from, _to, _amount); // Event log

        return true;
    }

    function burnFrom(address _from, uint256 _amount) public onlyICOReSale {
        require(_amount <= balances[_from]);

        balances[_from] = balances[_from].sub(_amount);
        totalSupply_ = totalSupply_.sub(_amount);
        emit Burn(_from, _amount);
        emit Transfer(_from, address(0), _amount);
    }

    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }


    // **** EVENTS
    
    // Triggered when tokens are transferred
    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    
    // Triggered when a wallet approves a spender to move its tokens
    event Approval(address indexed _tokenOwner, address indexed _spender, uint256 _amount);
    
    // Triggered when tokens are minted
    event Mint(address indexed _to, uint256 _amount);

    // Triggered when tokens are burned
    event Burn(address indexed burner, uint256 value);
}
