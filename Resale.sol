pragma solidity 0.4.24;

import "./TokensPreSale.sol";
import "./Tokens.sol";
import "./Multisign.sol";
import "./System.sol";

contract Resale is System {
    // **** DATA
        
    mapping (address => bool) public walletsAllowed;
    bool public ignoreWhitelist;
    uint256 public weisPerToken;

    address public ATokensNormal;
    address public ATokensPreSale;
    address public AMultisign;

    
    // **** FUNCTIONS
    constructor(address _ATokensNormal, address _ATokensPreSale, address _AMultisign) public {
        ATokensNormal = _ATokensNormal;
        ATokensPreSale = _ATokensPreSale;
        AMultisign = _AMultisign;
        weisPerToken = 1;
    }

    function transformToETH(uint256 _amount) public {
        uint256 balance = TokensPreSale(ATokensPreSale).balanceOf(msg.sender);
        require(_amount<=balance);

        uint256 weisToSend = _amount.mul(weisPerToken);
        require(weisToSend<=contractBalance());

        (msg.sender).transfer(weisToSend);
        TokensPreSale(ATokensPreSale).burnFrom(msg.sender, _amount);
    }


    function transformToToken(uint256 _amount) public {
        uint256 balance = TokensPreSale(ATokensPreSale).balanceOf(msg.sender);
        require(_amount<=balance);

        Tokens(ATokensNormal).mint(_amount);
        Tokens(ATokensNormal).transfer(msg.sender, _amount);
        TokensPreSale(ATokensPreSale).burnFrom(msg.sender, _amount);
    }

    function setWeisPerToken(uint256 _weisPerToken) public onlyOwner {
        weisPerToken = _weisPerToken;
    }

    function withdraw(uint256 _amount) public onlyOwner {
        require(_amount <= address(this).balance );

        require(Multisign(AMultisign).deposit.value(_amount)());
    }

    function() public payable {}


    // **** EVENTS

    event ChangeToken(address indexed _wallet, uint256 _amount);

    event ChangeETH(address indexed _wallet, uint256 _amount);

    event WithdrawETH(address indexed _wallet, uint256 _amount);

}

