pragma solidity 0.4.24;

import "./ICO.sol";


/**
 * @title ICOPreSale
 * @notice This contract is used to sell tokens to investors
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract ICOPreSale is ICO {
    // **** DATA

    mapping (address => uint256) private discounts;

    
    // **** FUNCTIONS
    
    /**
     * @notice ICO constructor overload
     */
    constructor() public {
        bigTokensHardCap = 10000000;
        tokensHardCap = bigTokensHardCap.mul(multiplier);
        weisHardCap = weisPerBigToken.mul(bigTokensHardCap);
    }

    /**
     * @notice Called by buyTokensLowLevel
     */
    function getDiscount() private view returns (uint256) {
        return discounts[msg.sender];
    }

    /**
     * @notice Called by owner to add custom discounts to addresses
     */
    function setDiscount(address _investor, uint256 _discountPerMille) public onlyOwner {
        require(_discountPerMille < 1000);
        discounts[_investor] = _discountPerMille;
    }

}
