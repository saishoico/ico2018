pragma solidity 0.4.24;

import "./Bundle.sol";

/**
 * @title Whitelist
 * @notice This contract is used to apply KYC/AML policies
 * @notice and aprove wallets before owning any tokens
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract Whitelist is Bundle {
    // **** DATA
        
    mapping (address => bool) public walletsAllowed;
    bool public ignoreWhitelist;

    address public AICONormal;
    address public AICOPreSale;
    address public AEscrowNormal;
    address public AEscrowPreSale;

    
    // **** FUNCTIONS
    constructor(address _AICONormal, address _AICOPreSale, address _AEscrowNormal, address _AEscrowPreSale) public {
        AICONormal = _AICONormal;
        AICOPreSale = _AICOPreSale;
        AEscrowNormal = _AEscrowNormal;
        AEscrowPreSale = _AEscrowPreSale;

        // Allow walletFounder1, otherwise it won't be able to get 10% of tokens when finalizing ICO
        walletsAllowed[walletFounder1] = true;
        emit AllowWallet(walletFounder1, block.timestamp); // Event log

        // Allow walletFiat, otherwise it won't be able to get premined tokens
        walletsAllowed[walletFiat] = true;
        emit AllowWallet(walletFiat, block.timestamp); // Event log

        // Allow Escrow contracts, otherwise it won't be able to store investors' tokens before 2nd check
        walletsAllowed[AEscrowNormal] = true;
        emit AllowWallet(AEscrowNormal, block.timestamp); // Event log
        walletsAllowed[AEscrowPreSale] = true;
        emit AllowWallet(AEscrowPreSale, block.timestamp); // Event log

        // Allow ICO contracts, otherwise it won't be able to get back tokens on investor refunds
        walletsAllowed[AICONormal] = true;
        emit AllowWallet(AICONormal, block.timestamp); // Event log
        walletsAllowed[AICOPreSale] = true;
        emit AllowWallet(AICOPreSale, block.timestamp); // Event log

        // Just for testing purpuses, add owner as an investor to the Whitelist
        walletsAllowed[owner] = true;
        emit AllowWallet(owner, block.timestamp); // Event log
    }

        // Checks whether a given wallet is authorized to own tokens
    function isWhitelisted(address _wallet) public view returns (bool) {
        if (ignoreWhitelist) {
            return true;
        }
        
        return (walletsAllowed[_wallet]);
    }

    /**
     * @notice Registers an investor
     * @return true if wallet is registered successfully
     */
    function allowWallet(address _wallet) public onlyComplianceOfficer returns (bool) {
        // Checks whether this wallet has been previously added as allowed
        require(!walletsAllowed[_wallet]);

        walletsAllowed[_wallet] = true;

        emit AllowWallet(_wallet, block.timestamp); // Event log
        
        return true;
    }

    /**
     * @notice Oracle can force to ignore the whitelist filter 
     */
    function setIgnoreWhitelist(bool _ignoreWhitelist) external onlyComplianceOfficer returns (bool) {
        ignoreWhitelist = _ignoreWhitelist;

        emit SetIgnoreWhitelist(_ignoreWhitelist, block.timestamp); // Event log
        
        return true;
    }

    // **** EVENTS

    // Triggered when a wallet is allowed to own the token
    event AllowWallet(address indexed _wallet, uint256 _timestamp);
    
    // Triggered when the owner changes the ignoreWhitelist value
    event SetIgnoreWhitelist(bool _ignoreWhitelist, uint256 _timestamp);
}
