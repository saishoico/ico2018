pragma solidity 0.4.24;

interface ITokens {

    ///
    function totalSupply() external view returns (uint256);

    /**
     * @notice Send _amount amount of tokens to address _to
     * @return true if transfer was successful
     */
    function transfer(address _to, uint256 _amount) external returns (bool);
    function transferFrom(address _from, address _to, uint256 _amount) external returns (bool);

    /**
    * @notice Function to mint tokens
    * @param _amount The amount of tokens to mint.
    * @return true if operation was successful.
    */
    function mint(uint256 _amount) external returns (bool);


    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    event Mint(address indexed _to, uint256 _amount);

}
