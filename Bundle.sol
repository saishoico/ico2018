pragma solidity 0.4.24;

import "./System.sol";


/**
 * @title Bundle
 * @notice This abstract contract is used to define oracle wallets and set linked contracts
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract Bundle is Haltable {
    // **** DATA
    
    // Hardcoded wallets
    address public walletComplianceOfficer; // Who validates a wallet
    address public walletFounder1;          // Founder #1 wallet, CEO, compulsory
    address public walletFounder2;          // Founder #2 wallet
    address public walletFounder3;          // Founder #3 wallet
    address public walletFounder4;          // Founder #4 wallet
    address public walletFounder5;          // Founder #5 wallet
    address public walletFiat;                // Wallet to store reserved tokens for Fiat ICO
    
    // Linked Contracts
    address public AWhitelist          ;    // The whitelist of allowed wallets to buy tokens on ICO
    address public ATokens             ;    // The token being sold
    address public AMultisign          ;    // The multisign wallet for founders to withdraw funds on consensus
    address public ARefundVault        ;    // The refund vault for softCap on ICO
    address public AEscrow             ;    // The escrow contract that stores tokens and funds of ICO investors on ICO
    address public AICO                ;    // Reference to ICO contract
    address public AReSale             ;    // Reference to resale contract


    // **** MODIFIERS

    // @notice To limit access to only Compliance Officer
    modifier onlyComplianceOfficer() {
        require(msg.sender == walletComplianceOfficer);
        _;
    }

    // @notice To limit access to only Escrow contract
    modifier onlyEscrow() {
        require(msg.sender == AEscrow);
        _;
    }

    // @notice To limit access to only linked ICO contract and Fiat wallet (instead of ICO Fiat contract)
    modifier onlyICO() {
        require(msg.sender == AICO || msg.sender == walletFiat);
        _;
    }

    // @notice To limit access to only linked ICO contract and Fiat wallet (instead of ICO Fiat contract)
    modifier onlyICOReSale() {
        require(msg.sender == AICO || msg.sender == walletFiat || msg.sender == AReSale);
        _;
    }


    // **** FUNCTIONS
    
    /**
     * @notice Constructor, set up the compliance officer oracle wallet
     */
    constructor() public {
        walletComplianceOfficer = 0x6F728110E45C1aD76cb5Ac8F7dED3308E8638a5B; // Who validates a wallet
        // set up the founders' oracle wallets
        walletFounder1 = 0xA3894Db3bbF4478bFefc557534181bBA87762185; // founder #1 (CEO) wallet
        walletFounder2 = 0xfC727B2f74eC1B6a9B430490616d31dC103cd257; // founder #2 wallet
        walletFounder3 = 0x7f1D98D5CaF9f85cc210E02584f7486F942f21e3; // founder #3 wallet
        walletFounder4 = 0xc7963C0e2724a9E2951d8f305bae5888eB144522; // founder #4 wallet
        walletFounder5 = 0xa3Be30bbfe20A0263C684B77D1AB33e71Bb86525; // founder #5 wallet
        walletFiat = 0xe434322d0723e224aDC3Ee0Fa1E215475570C346; // ICO Fiat wallet
    }
    
    /**
     * @notice After deployment of ICO, owner must set all contract addresses of the bundle on every contract
     * @notice This function is meant to be overloaded through inheritance on those SCs that need relative references of their respective ICO/Refund/Escrow contracts
     */    
    function setLinkedContracts(
        address _AWhitelist,
        address _ATokens,
        address _AMultisign,
        address _ARefundVault,
        address _AEscrow,
        address _AICO,
        address _AReSale)
    public
    onlyOwner {
        AWhitelist = _AWhitelist;
        ATokens = _ATokens;
        AMultisign = _AMultisign;
        ARefundVault = _ARefundVault;
        AEscrow = _AEscrow;
        AICO = _AICO;
        AReSale = _AReSale;
    }
}
