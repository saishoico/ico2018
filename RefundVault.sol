pragma solidity 0.4.24;

import "./Bundle.sol";
import "./Multisign.sol";
import "./ITokens.sol";
import "./ICO.sol";


/**
 * @title RefundVault
 * @notice This contract is used for storing funds while an ICO
 * @notice is in progress. Supports refunding the money if softCap not reached,
 * @notice and forwarding it if ICO is successfully closed.
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract RefundVault is Bundle {
    // **** DATA
    mapping (address => uint256) public deposited;
    mapping (address => uint256) public tokensAcquired;
    enum State { Active, Refunding, Closed }
    State public state;
    

    // **** FUNCTIONS
    
    /**
     * @notice Constructor
     */
    constructor() public {
        state = State.Active;
    }

    /**
     * @notice Registers how many tokens have each investor and how many ethers they spent (When ICOing through PayIn this function is not called)
     * @return true unless RefundVault state is not active
     */
    function deposit(address _investor, uint256 _tokenAmount) onlyEscrow public payable returns (bool) {
        deposited[_investor] = deposited[_investor].add(msg.value);
        tokensAcquired[_investor] = tokensAcquired[_investor].add(_tokenAmount);
        
        return true;
    }

    /**
     * @notice When ICO finalizes funds are transferred to multisign contract
     * @return true unless RefundVault state is not active or funds transfer fails
     */
    function close() onlyICO public returns (bool) {
        require(state == State.Active);
        state = State.Closed;

        // Transfer all funds
        require(Multisign(AMultisign).deposit.value(address(this).balance)());
        
        emit Closed(block.timestamp); // Event log
        
        return true;
    }

    /**
     * @notice Owner can ask to transfer an amount of funds to Multisign even though ICO is not finalized yet
     * @return false on any failure
     */
    function takeOut(uint256 _weiAmount) onlyOwner public returns (bool) {
        // Check whether contract has enough balance
        require(address(this).balance >= _weiAmount);
        
        // Check whether softCap has been reached
        require(ICO(AICO).tokensSold() >= ICO(AICO).tokensSoftCap());
        
        // Transfer the requested funds
        require(Multisign(AMultisign).deposit.value(_weiAmount)());
        
        emit TakeOut(_weiAmount, block.timestamp); // Event log
        
        return true;
    }

    /**
     * @notice When ICO finalizes, toggles refunding
     * @return true unless RefundVault state is not active
     */
    function enableRefunds() onlyICO public returns (bool) {
        require(state == State.Active);
        state = State.Refunding;
        
        emit RefundsEnabled(block.timestamp); // Event log
        
        return true;
    }
    
    /**
     * @notice Before transferring the ETHs to the investor, get back the tokens bought on ICO
     * @notice Investor must allow RefundVault contract to manage its tokens beforehand
     * @notice by using ERC-20 method approve(walletInvestor, allOfItsTokens)
     * @return the amount of tokens that has been sent back from the investor to the ICO
     */
    function getBackTokensOnRefund(address _investor) private returns (uint256 _tokenAmount) {
        _tokenAmount = tokensAcquired[_investor];
        require(ITokens(ATokens).transferFrom(_investor, AICO, _tokenAmount));
        emit GetBackTokensOnRefund(_investor, AICO, _tokenAmount, block.timestamp); // Event Log
        
        return _tokenAmount;
    }


    /**
     * @notice low level refunding function
     * @return the amount of weis that has been refunded to the investor
     */
    function refund(address _investor) private returns (uint256 _weisRefunded) {
        require(state == State.Refunding);
        require(deposited[_investor] > 0);
        _weisRefunded = deposited[_investor];
        
        // reset internal balances
        deposited[_investor] = 0;
        tokensAcquired[_investor] = 0; // tokens should have been returned previously to the ICO
        
        _investor.transfer(_weisRefunded);

        emit Refunded(_investor, _weisRefunded); // Event log
        
        return _weisRefunded;
    }
    
    /**
     * @notice If ICO is unsuccessful, investors can claim refunds here
     * @return false on any possible failure
     */
    function claimRefund() public stopInEmergency returns (bool) {
        require(state == State.Refunding);
        
        // Before transfering the ETHs to the investor, get back the tokens bought on ICO        
        uint256 _tokenAmount = getBackTokensOnRefund(msg.sender);
        require(_tokenAmount > 0);

        uint256 _weisRefunded = refund(msg.sender);
        require(_weisRefunded > 0);

        return true;
    }


    /**
     * @notice To allow ICO contracts to check whether RefundVault is ready to refund investors
     * @return true if RefundVault is on Refunding state
     */
    function isRefunding() public view returns (bool) {
        return (state == State.Refunding);
    }

    
    
    // **** EVENTS
    
    // Triggered when ICO contract closes the vault and send funds to the founders multisign
    event Closed(uint256 _timestamp);
    
    // Triggered when ICO contract asks to take out some funds to the founders multisign
    event TakeOut(uint256 _weiAmount, uint256 _timestamp);
    
    // Triggered when ICO contract initiates refunding
    event RefundsEnabled(uint256 _timestamp);
    
    // Triggered when an investor claims (through ICO contract) and gets its funds
    event Refunded(address indexed _beneficiary, uint256 _weiAmount);
    
    // Triggered when investor refunds and their tokens got back to ICO contract
    event GetBackTokensOnRefund(address _from, address _to, uint256 _amount, uint256 _timestamp);
}
