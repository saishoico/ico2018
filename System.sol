// System.sol

/*
MarketPay Solidity Libraries
developed by:
    MarketPay.io , 2018
    https://marketpay.io/
    https://goo.gl/kdECQu

v1.3 
    + Removed WhoAmI, timestamp and error functions for gas saving on deployment
    + Added revert and info error on SafeMath
    + Contract owner is public (to check on deployments, mainly)
    
v1.2 
    + Haltable by SC owner
    + Constructors upgraded to new syntax
    
v1.1 
    + Upgraded to Solidity 0.4.22
    
v1.0
    + System functions

*/

pragma solidity 0.4.24;

/**
 * @title SafeMath
 * @dev Math operations with safety checks that revert on error
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
        // Gas optimization: this is cheaper than asserting 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        // uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return a / b;
    }

    /**
    * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
        c = a + b;
        assert(c >= a);
        return c;
    }
}
/**
 * @title System
 * @dev Abstract contract that includes some useful generic functions.
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract System {
    using SafeMath for uint256;
    
    address public owner;

    
    // **** MODIFIERS

    // @notice To limit functions usage to contract owner
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    
    
    // **** FUNCTIONS
    
    // @notice Get the balance in weis of this contract
    function contractBalance() public view returns (uint256) {
        return address(this).balance;
    }
    
    // @notice System constructor, defines owner
    constructor() public {
        // This is the constructor, so owner should be equal to msg.sender, and this method should be called just once
        owner = msg.sender;
    }
    
    function updateOwner(address _owner) public onlyOwner {
        owner = _owner;
    }
}

/**
 * @title Haltable
 * @dev Abstract contract that allows children to implement an emergency stop mechanism.
 */
contract Haltable is System {
    bool public halted;

    
    // **** MODIFIERS

    modifier stopInEmergency {
        require(!halted);
        _;
    }

    modifier onlyInEmergency {
        require(halted);
        _;
    }

    
    // **** FUNCTIONS
    
    // called by the owner on emergency, triggers stopped state
    function halt() external onlyOwner {
        halted = true;
        emit Halt(true, msg.sender, block.timestamp); // Event log
    }

    // called by the owner on end of emergency, returns to normal state
    function unhalt() external onlyOwner onlyInEmergency {
        halted = false;
        emit Halt(false, msg.sender, block.timestamp); // Event log
    }

    
    // **** EVENTS
    // @notice Triggered when owner halts contract
    event Halt(bool _switch, address _halter, uint256 _timestamp);
}
