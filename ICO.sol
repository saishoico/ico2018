pragma solidity 0.4.24;

import "./Bundle.sol";
import "./Whitelist.sol";
import "./Escrow.sol";
import "./RefundVault.sol";
import "./ITokens.sol";


/**
 * @title ICO
 * @notice This contract is used to sell tokens to investors
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract ICO is Bundle {
    // **** DATA

    // start and end timestamps where investments are allowed (both inclusive)
    uint256 public startTime;
    uint256 public endTime;
    bool public isFinalized = false;

    uint256 public weisPerBigToken; // how many weis a buyer pays to get a big token (10^18 tokens) 
    uint256 public weisPerEther;
    uint256 public eurosPerEther;

    uint256 public multiplier; // 10^decimals
    uint256 public weisRaised; // amount of Weis raised
    uint256 public tokensSold; // amount of tokens sold during the ICO
    uint256 public tokensHardCap; // Max amount of Tokens for sale w/ multiplier
    uint256 public bigTokensHardCap; // Max amount of Tokens for sale w/o multiplier
    uint256 public weisHardCap; // Max amount of Weis raised
    uint256 public weisMinInvestment; // Min amount of Weis to perform a token sale
    uint256 public tokensSoftCap; // Min amount of Tokens to sale on success w/ multiplier
    uint256 public bigTokensSoftCap; // Min amount of Tokens to sale on success w/o multiplier
    uint256 public eurosSoftCap; // Min amount of Euros  for sale to ICO become successful
    uint256 public weisSoftCap; // Min amount of Weis raised to ICO become successful
    
    

    // **** FUNCTIONS

    /**
     * @notice ICO constructor. Definition of ICO parameters and set subcontract addresses 
     */
    constructor() public {
        // ICO parameters
        weisPerEther = 1 ether; // watch out! ether is just a 10^18 multiplier, not a coin
        multiplier = 1;
        
        // Deadline
        startTime = block.timestamp;
        endTime = startTime.add(365 days); // "This subphase will be open for investors for 365 days before the ICO"
                
        // Default Ether rate
        updateEurosPerEther(250); // Sets weisPerBigToken and weisMinInvestment too
        
        // Token Price specified and updated at updateEurosPerEther()
        
        // HardCap
        bigTokensHardCap = 0; // No hardcap on normal ICO
        tokensHardCap = bigTokensHardCap.mul(multiplier);
        weisHardCap = weisPerBigToken.mul(bigTokensHardCap);
        
        // SoftCap
        bigTokensSoftCap = 81018;
        tokensSoftCap = bigTokensSoftCap.mul(multiplier);
        weisSoftCap = weisPerBigToken.mul(bigTokensSoftCap);
    }
    
    // fallback function can be used to buy tokens
    function () payable public {
        buyTokens();
    }
    
    /**
     * @notice Token purchase function, funds and tokens are temporalily stored on Escrow contract
     * @notice Beneficiary (who gets tokens) = msg.sender = Escrow contract
     * @notice Purchaser (who paid with Ethers) = investor wallet
     * @return tokenAmount purchased
     */
    function buyTokens() public stopInEmergency payable returns (uint256 _tokenAmount) {
        require(msg.value > 0);

        // Send tokens to beneficiary
        _tokenAmount = buyTokensLowLevel(msg.sender, AEscrow, msg.value);
        
        // Send the investor's ethers to the Escrow contract
        require(Escrow(AEscrow).deposit.value(msg.value)(msg.sender, _tokenAmount));
        
        emit BuyTokens(msg.sender, AEscrow, msg.value, _tokenAmount, block.timestamp); // Event log

        return _tokenAmount;
    }

    /**
     * @notice Low level token purchase function, w/o ether transfer from investor
     * @return the number of tokens acquired. On failure, just throws to revert value
     * @param _purchaser who is paying, final investor wallet
     * @param _beneficiary who is getting the tokens, Escrow contract
     */
    function buyTokensLowLevel(
        address _purchaser,
        address _beneficiary,
        uint256 _weiAmount)
    private stopInEmergency returns (uint256 tokenAmount) {
        require(_purchaser != address(0));
        require(_beneficiary != address(0));
        require(block.timestamp >= startTime && block.timestamp <= endTime);
        require(Whitelist(AWhitelist).isWhitelisted(_purchaser));
        require(Whitelist(AWhitelist).isWhitelisted(_beneficiary));
        require(!isFinalized);
        require(_weiAmount >= weisMinInvestment);
        require(weisRaised.add(_weiAmount) <= weisHardCap || weisHardCap == 0);
        
        // Calculate the discount (per mille) to apply depending on token totalSupply (stepped auction)
        uint256 discount = getDiscount();

        // Calculate the per mille price to apply as an integer
        uint256 discountedPricePerMille = 1000;
        discountedPricePerMille = discountedPricePerMille.sub(discount);
        
        // Calculate token amount to be sold applying discount
        tokenAmount = _weiAmount.mul(multiplier).mul(1000).div(discountedPricePerMille).div(weisPerBigToken);

        // Update fund counter
        weisRaised = weisRaised.add(_weiAmount);
        
        // Mint the tokens
        ITokens(ATokens).mint(tokenAmount);

        // Send the tokens to the _beneficiary
        require(ITokens(ATokens).transfer(_beneficiary, tokenAmount));
        
        // Update sold token counter
        tokensSold = tokensSold.add(tokenAmount);
        
        emit BuyTokensLowLevel(_purchaser, _beneficiary, _weiAmount, tokenAmount, block.timestamp); // Event log

        return tokenAmount;
    }

    /**
     * @notice Called by buyTokensLowLevel
     */
    function getDiscount() private view returns (uint256) {

        if (ITokens(ATokens).totalSupply() < 2500000) {
            return 100;
        } else if (ITokens(ATokens).totalSupply() < 7500000) {
            return 75;
        } else if (ITokens(ATokens).totalSupply() < 15000000) {
            return 50;
        } else if (ITokens(ATokens).totalSupply() < 30000000) {
            return 25;
        }
        return 0;
    }

    
    /**
     * @notice Called by owner to alter the ICO deadline
     */
    function updateEndTime(uint256 _endTime) onlyOwner public returns (bool) {
        endTime = _endTime;
        
        emit UpdateEndTime(_endTime); // Event log
        return true;
    }

    /**
     * @notice Called by owner to update the eurosPerEther value
     */
    function updateEurosPerEther(uint256 _eurosPerEther) onlyOwner public returns (bool) {
        eurosPerEther = _eurosPerEther;
        
        // Update linked values too
        weisPerBigToken = weisPerEther.div(eurosPerEther); // ST price is 1 euro
        weisMinInvestment = weisPerEther.div(eurosPerEther); // Minimal Investment, 1 euro

        emit UpdateEurosPerEther(_eurosPerEther); // Event log
        
        return true;
    }
    
    /**
     * @notice Must be called by owner before or after crowdsale ends, to check whether soft cap is reached and transfer collected funds
     * @return false on any possible failure
     */
    function finalize() onlyOwner public returns (bool) {
        require(!isFinalized);
        isFinalized = true;


        if (weisRaised >= weisSoftCap) {
            require(RefundVault(ARefundVault).close());
            
            // Send a bonus of 10% of ICO tokens to founder#1
            uint256 tokenAmount = tokensSold.mul(10).div(100); // 10% of tokensSold
            ITokens(ATokens).mint(tokenAmount); // Mint the tokens
            require(ITokens(ATokens).transfer(walletFounder1, tokenAmount));
        } else {
            require(RefundVault(ARefundVault).enableRefunds());
        }
        
        emit Finalized(block.timestamp); // Event log
        
        return true;
    }


    // **** EVENTS
    
    // Triggered when an investor buys some tokens directly with Ethers
    event BuyTokens(address indexed _purchaser, address indexed _beneficiary, uint256 _value, uint256 _tokenAmount, uint256 _timestamp);
        
    // Triggered when an investor buys some tokens directly with Ethers or through payin Oracle
    event BuyTokensLowLevel(address indexed _purchaser, address indexed _beneficiary, uint256 _value, uint256 _tokenAmount, uint256 _timestamp);
    
    // Triggered when an SC owner requests to end the ICO, transferring funds to founders wallet or offering them as a refund
    event Finalized(uint256 _timestamp);
    
    // Triggered when Owner updates ICO deadlines
    event UpdateEndTime(uint256 _endTime);
    
    // Triggered when Owner updates the rate euros/ether
    event UpdateEurosPerEther(uint256 _eurosPerEther);
}
