pragma solidity 0.4.24;

import "./Bundle.sol";
import "./Tokens.sol";
import "./RefundVault.sol";


/**
 * @title Escrow
 * @notice This contract is used to store ICO tokens and ethers until
 * @notice ComplianceOfficer oracle releases or revokes the investor escrow
 * @author https://marketpay.io/ & https://goo.gl/kdECQu
 */
contract Escrow is Bundle {
	// **** DATA

	mapping (address => uint256) public tokensOnEscrow;
	mapping (address => uint256) public weisInvested;

	
	// **** FUNCTIONS
	
	/**
	 * @notice Registers how many tokens have each investor and how many ethers they spent
	 * @return true
	 */
	function deposit(address _investor, uint256 _tokenAmount) onlyICO public payable returns (bool) {
		weisInvested[_investor] = weisInvested[msg.sender].add(msg.value);
		tokensOnEscrow[_investor] = tokensOnEscrow[msg.sender].add(_tokenAmount);
		
		emit Deposit(_investor, msg.value, _tokenAmount, block.timestamp);

		return true;
	}

	/**
	 * @notice Releases the escrow, sending the tokens to the final investor and funds to RefundVault contract
	 * @return true if successfully transferred the tokens and funds
	 */
	function releaseEscrow(address _wallet) public onlyComplianceOfficer returns (bool) {
		uint256 tokenAmount = tokensOnEscrow[_wallet];
		uint256 weiAmount = weisInvested[_wallet];
		
		// Checks whether this wallet has any tokens on escrow
		require(tokenAmount > 0);
		
		// Reset tokens and weis escrow balances
		tokensOnEscrow[_wallet] = 0;
		weisInvested[_wallet] = 0;

		// Transfer the tokens on escrow to the investor's wallet
		require(Tokens(ATokens).transfer(_wallet, tokenAmount));

		// Transfer the funds on escrow to the RefundVault contract
		require(RefundVault(ARefundVault).deposit.value(weiAmount)(_wallet, tokenAmount));

		emit ReleaseEscrow(_wallet, tokenAmount, weiAmount, block.timestamp); // Event log
		
		return true;
	}

	/**
	 * @notice Revokes the token escrow, sending the tokens back to the ICO contract
	 * @notice and funds to investor
	 * @return true if successfully transferred back the tokens to ICO and funds to investor
	 */
	function revokeEscrow(address _wallet) public onlyComplianceOfficer returns (bool) {
		uint256 tokenAmount = tokensOnEscrow[_wallet];
		uint256 weiAmount = weisInvested[_wallet];
		
		// Checks whether this wallet has any tokens on escrow
		require(tokenAmount > 0);
		
		// Reset tokens and weis escrow balances
		tokensOnEscrow[_wallet] = 0;
		weisInvested[_wallet] = 0;

		// Transfer the tokens on escrow to the ICO contract
		require(Tokens(ATokens).transfer(AICO, tokenAmount));

		// Transfer the funds on escrow to the investor
		_wallet.transfer(weiAmount);

		emit RevokeEscrow(_wallet, tokenAmount, weiAmount, block.timestamp); // Event log
		
		return true;
	}
	



	// **** EVENTS
		
	// Triggered when an ICO sends investor's tokens and funds on Escrow
	event Deposit(address _investor, uint256 _weiAmount, uint256 _tokenAmount, uint256 _timestamp);

	// Triggered when ComplianceOfficer oracle approves investor's second tick, releasing escrow of tokens
	event ReleaseEscrow(address indexed _investor, uint256 _tokenAmount, uint256 _weiAmount, uint256 _timestamp);
	
	// Triggered when ComplianceOfficer oracle revokes investor's second tick
	event RevokeEscrow(address indexed _investor, uint256 _tokenAmount, uint256 _weiAmount, uint256 _timestamp);
}
